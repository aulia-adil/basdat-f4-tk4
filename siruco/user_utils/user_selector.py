from siruco.functions.functions import execute_query


def execute_query_with_one_attribute(query):
    tuple_result = execute_query(query)
    list_result = []
    for element in tuple_result:
        list_result.append(element[0])
    return list_result


def get_all_username(table):
    query = ("SELECT username FROM {}").format(table)
    return execute_query_with_one_attribute(query)


def get_id_faskes_admin_satgas(username):
    query = ("SELECT idfaskes FROM ADMIN_SATGAS "
             "WHERE username={}").format(username)
    return execute_query_with_one_attribute(query)


def get_all_data_from_table_with_key(key, key_column_name, table):
    query = ("SELECT * FROM {} "
             "WHERE {}='{}'").format(table, key_column_name, key)
    return execute_query(query)