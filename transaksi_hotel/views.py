from django.http import response
from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt

def display_transaksi_hotel(request):
    username = request.session['username']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    if(request.session['authority'] == 'pengguna publik'):
        cursor.execute("select * from transaksi_hotel t join pasien p on p.nik = t.kodepasien join pengguna_publik pp on p.idpendaftar = pp.username where pp.username=%s;", [username])
    else:
        cursor.execute("select * from transaksi_hotel")
    transaksi_hotel_response = dictfetch(cursor)
    context = {'apt' : transaksi_hotel_response}
    return render(request, 'transaksi_hotel.html', context)

def detail_transaksi_hotel(request,id):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select * from transaksi_hotel t join pasien p on p.nik = t.kodepasien join pengguna_publik pp on p.idpendaftar = pp.username where t.idtransaksi=%s;", [id])
    detail_transaksi_hotel = dictfetch(cursor)
    tb = get_transaksi_booking(id)
    tm = get_transaksi_makan(id)
    rh = get_reservasi_hotel(id)
    if '' not in (detail_transaksi_hotel, tb, tm, rh):
        context = {'apt' : detail_transaksi_hotel[0], 'tb' : tb[0], 'tm' : tm, 'rh' : rh[0]}
    return render(request, 'detail_transaksi_hotel.html', context)

def update_transaksi_hotel(request,id):
    username = request.session['username']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("update transaksi_hotel set statusbayar = 'Lunas', tanggalpembayaran = now(), waktupembayaran = date_trunc('hour',now()) where idtransaksi=%s;",[id])
    cursor.execute("select * from transaksi_hotel t join pasien p on p.nik = t.kodepasien join pengguna_publik pp on p.idpendaftar = pp.username where pp.username=%s;", [username])
    transaksi_hotel_response = dictfetch(cursor)
    context = {'apt' : transaksi_hotel_response}
    return render (request,'transaksi_hotel.html', context)

def get_transaksi_booking(id):
    cursor = connection.cursor()
    cursor.execute("select totalbayar from transaksi_booking where idtransaksibooking=%s;",[id])
    get_tb_response = dictfetch(cursor)
    return get_tb_response 

def get_transaksi_makan(id):
    cursor = connection.cursor()
    cursor.execute("select pm.harga from paket_makan pm join daftar_pesan dp on pm.kodepaket = dp.kodepaket where dp.id_transaksi=%s;",[id])
    transaksi_makan_response = dictfetch(cursor)
    return transaksi_makan_response

def get_reservasi_hotel(id):
    cursor = connection.cursor()
    cursor.execute("select * from reservasi_hotel rh join hotel h on rh.kodehotel = h.kode join transaksi_hotel th on rh.kodepasien = th.kodepasien where th.idtransaksi=%s;",[id])
    reservasi_hotel_response = dictfetch(cursor)
    return reservasi_hotel_response

# https://docs.djangoproject.com/en/3.2/topics/db/sql/
def dictfetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]