from django.contrib.auth.models import User
from django.db import connection, IntegrityError



def delete_all_django_user():
    User.objects.all().delete()


def create_django_user(username, password):
    User.objects.create_user(username=username,
                             email=username,
                             password=password)


def init_user():
    # delete_all_django_user()

    query = "SELECT username, password FROM AKUN_PENGGUNA"

    cursor = connection.cursor()
    cursor.execute(query)
    query_result = cursor.fetchall()

    for data in query_result:
        try:
            create_django_user(data[0], data[1])
        except IntegrityError:
            pass



