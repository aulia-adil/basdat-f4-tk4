from django.urls import path

from crjadwaldokter.views import buat_jadwal_dokter_view, list_jadwal_dokter_view, buat_jadwal_dokter_app

urlpatterns = [
    path('buat-jadwal-dokter/', buat_jadwal_dokter_view, name='buat-jadwal-dokter'),
    path('buat-jadwal-dokter/buat/', buat_jadwal_dokter_app, name='buat-jadwal-dokter-app'),
    path('list-jadwal-dokter/', list_jadwal_dokter_view, name='list-jadwal-dokter'),
]
