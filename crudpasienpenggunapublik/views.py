from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect

from siruco.functions.functions import execute_query
from siruco.user_utils.user_creator import create_pasien
from siruco.user_utils import user_updater

REQUIRED_TOTAL_INPUT_MENDAFTARKAN = 15
REQUIRED_TOTAL_INPUT_UPDATE = 15
INPUT_NOT_COMPLETE_ERROR = \
    "Data yang diisikan " \
    "belum lengkap, silahkan " \
    "lengkapi data terlebih dahulu"


@login_required(login_url='login')
def pendaftaran_pasien_view(request):
    if request.session['authority'] != "pengguna publik":
        return HttpResponse(status=404)

    return render(request,
                  "crudpasienpenggunapublik/pendaftaran-pasien.html",
                  {"pendaftar": request.session['username']})


def isMendaftarkanPasienInputComplete(request):
    count = 0
    for value in request.POST.values():
        if value:
            count += 1
    return count == REQUIRED_TOTAL_INPUT_UPDATE


def isUpdatePasienInputComplete(request):
    count = 0
    for value in request.POST.values():
        if value:
            count += 1
    return count == REQUIRED_TOTAL_INPUT_MENDAFTARKAN


def mendaftarkan_pasien(request):
    if isMendaftarkanPasienInputComplete(request):
        create_pasien(
            request.POST['nik'],
            request.session['username'],
            request.POST['nama'],
            request.POST['jalan-ktp'],
            request.POST['kelurahan-ktp'],
            request.POST['kecamatan-ktp'],
            request.POST['kabkot-ktp'],
            request.POST['provinsi-ktp'],
            request.POST['jalan-dom'],
            request.POST['kelurahan-dom'],
            request.POST['kecamatan-dom'],
            request.POST['kabkot-dom'],
            request.POST['provinsi-dom'],
            request.POST['no-tlp'],
            request.POST['no-hp'],
        )
        return redirect("../list-pasien")
    else:
        messages.error(request, INPUT_NOT_COMPLETE_ERROR)
        return redirect("../pendaftaran-pasien/")


@login_required(login_url='login')
def list_pasien_view(request):
    if request.session['authority'] != "pengguna publik":
        return HttpResponse(status=404)
    pendaftar = request.session['username']
    list_pasien = get_pasien(pendaftar)
    return render(request, "crudpasienpenggunapublik/list-pasien.html", {"list_pasien": list_pasien})


def get_pasien(pendaftar):
    query = ("SELECT nik, nama FROM PASIEN "
             "WHERE idpendaftar='{}'").format(pendaftar)
    query_result = execute_query(query)
    result = []
    counter = 1
    for element in query_result:
        temp_list = []
        temp_list.append(counter)
        temp_list.append(element[0])
        temp_list.append(element[1])
        result.append(temp_list)
        counter += 1
    return result


def delete_pasien(request):
    nik = request.POST['delete-pasien-nik']
    query = ("DELETE FROM PASIEN "
             "WHERE nik='{}'").format(nik)
    execute_query(query)
    return redirect("/pengguna-publik/list-pasien/")


def detail_pasien(request):
    query = ("SELECT * FROM PASIEN "
             "WHERE nik='{}'").format(request.POST['detail-pasien-nik'])
    query_result = execute_query(query)[0]
    return render(request, "crudpasienpenggunapublik/detail-pasien.html", {"data": query_result})


@login_required(login_url='login')
def update_pasien_view(request):
    if request.session['authority'] != "pengguna publik":
        return HttpResponse(status=404)
    query = ("SELECT * FROM PASIEN "
             "WHERE nik='{}'").format(request.POST['update-pasien-nik'])
    query_result = execute_query(query)[0]
    return render(request, "crudpasienpenggunapublik/update-pasien.html", {"data": query_result})


def update_pasien_app(request):
    if isUpdatePasienInputComplete(request):
        user_updater.update_pasien(
            request.POST['NIK'],
            ktp_jalan=request.POST['jalan-ktp'],
            ktp_kelurahan=request.POST['kelurahan-ktp'],
            ktp_kecamatan=request.POST['kecamatan-ktp'],
            ktp_kabkot=request.POST['kabkot-ktp'],
            ktp_prov=request.POST['provinsi-ktp'],
            dom_jalan=request.POST['jalan-dom'],
            dom_kelurahan=request.POST['kelurahan-dom'],
            dom_kecamatan=request.POST['kecamatan-dom'],
            dom_kabkot=request.POST['kabkot-dom'],
            dom_prov=request.POST['provinsi-dom'],
            notelp=request.POST['no-tlp'],
            nohp=request.POST['no-hp']
        )
        return redirect("/pengguna-publik/list-pasien/")
    else:
        messages.error(request, INPUT_NOT_COMPLETE_ERROR)
        return update_pasien_view(request)
