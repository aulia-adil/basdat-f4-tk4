from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute("SELECT nik FROM siruco.pasien")
select1 = cursor.fetchall()
cursor.execute("SELECT kode_faskes FROM siruco.rumah_sakit")
select2 = cursor.fetchall()
cursor.execute("SELECT koderuangan FROM siruco.ruangan_rs")
select3 = cursor.fetchall()
cursor.execute("SELECT kodebed FROM siruco.bed_rs")
select4 = cursor.fetchall()

nik_choices = [tuple([select1[x][0], select1[x][0]]) for x in range(len(select1))]
kode_faskes_choices = [tuple([select2[x][0],select2[x][0]]) for x in range(len(select2))]
koderuangan_choices = [tuple([select3[x][0],select3[x][0]]) for x in range(len(select3))]
kodebed_choices = [tuple([select4[x][0],select4[x][0]]) for x in range(len(select4))]

class CreateReservasiRumahSakit(forms.Form):
    dropdown_pasien = forms.CharField(label="NIK Pasien ", widget=forms.Select(choices=nik_choices, attrs={
        'class': 'form-control'
    }))
    tanggal_masuk = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'tanggal masuk',
        'type': 'date',
        'required': True,
    }))
    tanggal_keluar = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'tanggal keluar',
        'type': 'date',
        'required': True,
    }))
    dropdown_kode_faskes = forms.CharField(label="kode faskes", widget=forms.Select(choices=kode_faskes_choices, attrs={
        'class': 'form-control'
    }))
    dropdown_koderuangan = forms.CharField(label="kode ruangan", widget=forms.Select(choices=koderuangan_choices, attrs={
        'class': 'form-control'
    }))
    dropdown_kodebed = forms.CharField(label="kode bed", widget=forms.Select(choices=kodebed_choices, attrs={
        'class': 'form-control'
    }))

class CreateFaskes(forms.Form):
    nama_faskes = forms.CharField(required = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Faskes',
        'type' : 'text',
    }))

    jalan = forms.CharField(required = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
    }))

    kelurahan = forms.CharField(required = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kelurahan',
        'type' : 'text',
    }))
    
    kecamatan = forms.CharField(required = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kecamatan',
        'type' : 'text',
    }))

    kabupaten_kota = forms.CharField(required = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kabupaten/ Kota',
        'type' : 'text',
    }))

    provinsi = forms.CharField(required = True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Provinsi',
        'type' : 'text',
    }))

cursor.execute("SELECT kode FROM siruco.faskes")
select5 = cursor.fetchall()

faskes_choices = [tuple([select5[x][0], select5[x][0]]) for x in range(len(select5))]

class CreateJadwalFaskes(forms.Form):
    dropdown_faskes = forms.CharField(label="Faskes", widget=forms.Select(choices=faskes_choices, attrs={
        'class': 'form-control'
    }))
    shift = forms.CharField(required = False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Shift',
        'type' : 'text',
    }))

    tanggal = forms.DateField(required = False, input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'tanggal',
        'type': 'date',
    }))

cursor.execute("SELECT kode FROM siruco.faskes")
selectdah = cursor.fetchall()
kode_faskes_from_faskes = [tuple([selectdah[x][0],selectdah[x][0]]) for x in range(len(selectdah))]

class CreateRumahSakitRujukan(forms.Form):
    dropdown_kode_faskes = forms.CharField(label="kode faskes", widget=forms.Select(choices=kode_faskes_from_faskes, attrs={
        'class': 'form-control'
    }))
    rujukan = forms.BooleanField(label= "rujukan", required=False, initial=False, help_text="", disabled=False)

class UpdateRumahSakitRujukan(forms.Form):
    rujukan = forms.BooleanField(label= "rujukan", required=False, initial=False, help_text="", disabled=False)


