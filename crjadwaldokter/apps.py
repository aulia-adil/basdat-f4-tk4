from django.apps import AppConfig


class CrjadwaldokterConfig(AppConfig):
    name = 'crjadwaldokter'
