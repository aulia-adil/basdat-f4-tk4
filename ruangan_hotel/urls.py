from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from . import views
from ruangan_hotel.views import booking_ruangan_hotel

app_name = 'ruangan_hotel'

urlpatterns = [
    path('booking_ruangan_hotel/',views.booking_ruangan_hotel, name='booking_ruangan_hotel'),
    path('list_ruangan_hotel/',views.show_ruangan_hotel, name='show_ruangan_hotel'),
    path('reservasi_hotel/',views.buat_reservasi_hotel, name='buat_reservasi_hotel'),
    path('list_reservasi_hotel/',views.show_reservasi_hotel, name='show_reservasi_hotel'),
    path('list_transaksi_hotel/',views.show_transaksi_hotel,name='show_transaksi_hotel'),
    path('list_transaksi_hotel/<str:id>',views.show_ubah_transaksi_hotel,name='show_ubah_transaksi_hotel'),
    path('list_transaksi_booking/',views.show_transaksi_booking,name='show_transaksi_booking'),
]
