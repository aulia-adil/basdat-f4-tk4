from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from . import views

app_name='hasiltespasien'

urlpatterns = [
    path('buathasiltes',views.buat_hasil_tes,name='buat-hasil-tes'),
    path('listhasiltes',views.list_hasil_tes, name='list-hasil-tes')
]