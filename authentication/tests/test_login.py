from django.test import TestCase
from django.urls import reverse

class BaseTest(TestCase):
    def setUp(self):
        self.register_url = reverse('register')
        self.login_url = reverse('login')
        self.user = {
            "username": "testusername",
            "password": "testpassword"
        }
        return super().setUp()

# class LoginTest(BaseTest):
#     def test_can_access_page(self):
#         response = self.client.get(self.login_url)
#         self.assertEqual(response.status_code, 200)
#
#     def test_login_page_is_true(self):
#         response = self.client.get(self.login_url)
#         self.assertTemplateUsed(response, 'authentication/login.html')
#
#     def test_login_success(self):
#         pass