from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from . import views

app_name='transaksi_rs'

urlpatterns = [
    path('transaksi_rs',views.display_transaksi_rs,name='display-transaksi-rs'),
    path('transaksi_rs/detail/<str:id>',views.detail_transaksi_rs,name='detail-transaksi-rs'),
    path('transaksi_rs/<str:id>',views.update_transaksi_rs,name='update-transaksi-rs'),
]