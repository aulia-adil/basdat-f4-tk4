"""siruco URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('authentication.urls')),
    path('trigger3/', include ('trigger3.urls')),
    path("", include('dashboard.urls')),
    path('transaksi/', include('transaksi_hotel.urls')),
    path('transaksi/', include('transaksi_rs.urls')),
    path('pasien/', include('hasiltespasien.urls')),
    path("hotel/", include('ruangan_hotel.urls')),
    path("", include('dashboard.urls')),
    path("hotel/", include('ruangan_hotel.urls')),
    path("pengguna-publik/", include('crudpasienpenggunapublik.urls')),
    path("jadwal-dokter/", include('crjadwaldokter.urls')),
    path("appointment/", include('crudappointmentwithdoctor.urls')),
    path("ruangan-bed/", include('crudruanganbed.urls')),
    path('pasien/', include('hasiltespasien.urls')),
    path('hotel/', include('hotel.urls'))
]
