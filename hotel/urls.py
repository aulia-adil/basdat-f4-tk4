from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from .views import *

app_name = 'hotel'

urlpatterns = [
    path('create/',
         create_hotel, name='create-hotel'),

    path('show/',
         show_hotel, name='show-hotel'),

    path('ubah/<str:kode>',
         ubah_hotel, name='ubah-hotel'),

    path('create-makan/',
         create_makan, name='create-makan'),

    path('show-makan/',
         show_makan, name='show-makan'),

    path('ubah-makan/<str:kode_hot>/<str:kode_pak>',
         ubah_makan, name='ubah-makan'),

    path('delete-makan/<str:kode_hot>/<str:kode_pak>',
         delete_makan, name='delete-makan'),

    path('create-trans-makan/',
         create_trans_makan, name='create-trans-makan'),

    path('show-trans-makan/',
         show_trans_makan, name='show-trans-makan'),

    path('ubah-trans-makan/',
         ubah_trans_makan, name='ubah-trans-makan'),

    path('detail-trans-makan/<str:idtr>/<str:idtm>',
         detail_trans_makan, name='detail-trans-makan'),

]
