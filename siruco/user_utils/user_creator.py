from siruco.functions.functions import execute_query


def create_pasien(nik, id_pendaftar, nama, ktp_jalan,
                  ktp_kelurahan, ktp_kecamatan, ktp_kabkot,
                  ktp_prov, dom_jalan, dom_kelurahan,
                  dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp):
    query = ("INSERT INTO PASIEN VALUES"
             "('{}', '{}', '{}', '{}',"
             " '{}', '{}', '{}', '{}',"
             " '{}', '{}', '{}', '{}',"
             " '{}', '{}', '{}')").format(
        nik, id_pendaftar, nama, ktp_jalan, ktp_kelurahan,
        ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan,
        dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov,
        notelp, nohp
    )
    execute_query(query)


def create_akun_pengguna(username, password, role):
    query = ("INSERT INTO AKUN_PENGGUNA VALUES"
             "('{}', '{}', '{}')").format(username, password, role)
    execute_query(query)


def create_admin_sistem(username):
    query = ("INSERT INTO ADMIN VALUES"
             " ('{}')").format(username)
    execute_query(query)


def create_pengguna_publik(username, nik, nama, no_hp):
    query = ("INSERT INTO PENGGUNA_PUBLIK VALUES"
             " ('{}', '{}', '{}', 'aktif',"
             "'pendaftar', '{}')").format(username, nik, nama, no_hp)
    execute_query(query)


def create_dokter(username, no_str, nama,
                  no_hp, gelar_depan, gelar_belakang):
    query = ("INSERT INTO DOKTER VALUES"
             " ('{}', '{}', '{}', '{}',"
             "'{}', '{}')").format(username, no_str, nama,
                                   no_hp, gelar_depan,
                                   gelar_belakang)
    execute_query(query)


def create_admin_satgas(username, kode_faskes):
    query = ("INSERT INTO ADMIN_SATGAS VALUES"
             " ('{}', '{}')").format(username, kode_faskes)
    execute_query(query)


def insert_data(table, **attributes):
    columns = "("
    values = "("
    for column, attribute in attributes.items():
        columns += column + ", "
        values += "'{}', ".format(attribute)
    columns = columns[0: len(columns)-2] + ")"
    values = values[0: len(values)-2] + ")"
    query = ("INSERT INTO {} "
             "{} VALUES {}").format(table, columns, values)
    execute_query(query)


