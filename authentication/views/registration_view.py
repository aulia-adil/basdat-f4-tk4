from django.contrib import messages
from django.db import InternalError, IntegrityError
from django.shortcuts import render, redirect

from authentication.registration import register_admin_sistem, register_pengguna_publik, register_dokter, \
    register_admin_satgas
from authentication.views.login_view import check_account
from siruco.functions.functions import check_password_equal_repeat_password, clean_string_from_select_query, \
    execute_query

WEAK_PASSWORD_ERROR_MESSAGE = \
    "Password Anda belum\
    memenuhi syarat, silahkan \
    pastikan bahwa password \
    minimal terdapat 1 huruf\
    kapital dan 1 angka"

DUPLICATE_KEY_ERROR_MESSAGE = \
    "Email sudah pernah digunakan,\
     silahkan pilih email yang lain"

REPEAT_PASSWORD_DIFFERENT_ERROR_MESSAGE = \
    "Kolom Password dan Ulangi Password " \
    "tidak sama"

def check_role_and_register(role, request):
    if role == "admin-sistem":
        register_admin_sistem(request)
    elif role == "pengguna-publik":
        register_pengguna_publik(request)
    elif role == "dokter":
        register_dokter(request)
    elif role == "admin-satgas":
        register_admin_satgas(request)


def registration_admin_sistem(request):
    return render(request, 'authentication/registration/registration-admin-sistem.html')


def registration_pengguna_publik(request):
    return render(request, 'authentication/registration/registration-pengguna-publik.html')


def registration_dokter(request):
    return render(request, 'authentication/registration/registration-dokter.html')


def registration_admin_satgas(request):
    query = "SELECT kode FROM FASKES"
    list_kode_faskes_dirty = execute_query(query)
    list_kode_faskes =\
        clean_string_from_select_query(list_kode_faskes_dirty)
    return render(request,
                  'authentication/registration/registration-admin-satgas.html',
                  {"list_kode_faskes": list_kode_faskes})


def register_user(request):
    role = request.POST.get("role")
    try:
        check_password_equal_repeat_password(request)
        check_role_and_register(role, request)
    except ValueError:
        messages.error(request, REPEAT_PASSWORD_DIFFERENT_ERROR_MESSAGE)
        return redirect('../register/' + role)
    except InternalError:
        messages.error(request, WEAK_PASSWORD_ERROR_MESSAGE)
        return redirect('../register/' + role)
    except IntegrityError:
        messages.error(request, DUPLICATE_KEY_ERROR_MESSAGE)
        return redirect('../register/' + role)
    return check_account(request)
