from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from siruco.functions.creator import create_session_data
from siruco.functions.functions import convert_request_session_to_dictionary
from siruco.user_utils.user_updater import update_user

@login_required(login_url='login')
def dashboard(request):
    authority = request.session["authority"]
    data = convert_request_session_to_dictionary(request)
    return render(request, 'dashboard/dashboard.html',
                  {"authority": authority,"user_data": data})


def update_personal_data(request):
    authority = request.POST['authority']
    if authority == "pengguna publik":
        update_user(request.session['username'],
                    "PENGGUNA_PUBLIK",
                    nik=request.POST['nik'],
                    nama=request.POST['nama'],
                    status=request.POST['status'],
                    peran=request.POST['peran'],
                    nohp=request.POST['no_hp'],)
    elif authority == "admin satgas":
        update_user(request.session['username'],
                    "ADMIN_SATGAS",
                    idfaskes=request.POST['id_faskes'])
    elif authority == "dokter":
        update_user(request.session['username'],
                    "ADMIN_SATGAS",
                    nostr=request.POST['no_str'],
                    nama=request.POST['nama'],
                    nohp=request.POST['no_hp'],
                    gelardepan=request.POST['gelar_depan'],
                    gelarbelakang=request.POST['gelar_belakang'])
    create_session_data(request, request.POST['username'])
    return redirect("../")

