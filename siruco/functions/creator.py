from siruco.functions.getter import get_user_data


def create_session_data(request, username):
    data = get_user_data(username)
    for key, value in data.items():
        request.session[key] = value