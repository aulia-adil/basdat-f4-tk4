from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from siruco.functions import delete_one_data_from_database, is_regex_match
from siruco.user_utils.user_creator import create_admin_sistem, create_akun_pengguna


class BaseTest(TestCase):
    def setUp(self):
        self.akun_pengguna_admin = {
            "username": "tes@tes.com",
            "password": "passTes1",
            "role": "admin"
        }
        self.admin = {
            "username": "tes@tes.com"
        }
        self.dashboard_url = reverse('dashboard')
        return super().setUp()


class DashboardTest(BaseTest):
    def test_can_access_page(self):
        response = self.client.get(self.dashboard_url)
        self.assertEqual(response.status_code, 200)

    def test_can_show_data_admin_sistem(self):
        create_akun_pengguna(self.akun_pengguna_admin['username'],
                             self.akun_pengguna_admin['password'],
                             self.akun_pengguna_admin['role'])
        create_admin_sistem(self.admin['username'])
        user = User.objects.create_user(self.akun_pengguna_admin['username'],
                                        self.akun_pengguna_admin['username'],
                                        self.akun_pengguna_admin['password'])

        self.client.force_login(username=self.akun_pengguna_admin['username'],
                          password=self.akun_pengguna_admin['username'])

        # user = self.client.force_login(User.objects.all()[0])
        response = self.client.get(self.dashboard_url)
        print(response.status_code)
        self.assertTrue(is_regex_match("Login", response.content))

    def tearDown(self):
        delete_one_data_from_database("ADMIN",
                                      "username",
                                      self.akun_pengguna_admin['username'])
        delete_one_data_from_database("AKUN_PENGGUNA",
                                      "username",
                                      self.akun_pengguna_admin['username'])
