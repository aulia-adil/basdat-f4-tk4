from django.http import response
from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
import datetime

#method ini cmn buat pengguna_publik
def display_transaksi_rs(request):
    username = request.session['username']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    if(request.session['authority'] == 'pengguna publik'):
        cursor.execute("select * from transaksi_rs t join pasien p on p.nik = t.kodepasien join pengguna_publik pp on p.idpendaftar = pp.username where pp.username=%s;", [username])
    else:
        cursor.execute("select * from transaksi_rs")

    transaksi_rs_response = dictfetch(cursor)
    context = {'apt' : transaksi_rs_response}
    # print(context)
    return render(request,'transaksi_rs.html', context)

def detail_transaksi_rs(request,id):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select * from transaksi_rs t natural join reservasi_rs r join faskes f on r.koders = f.kode where t.idtransaksi=%s;",[id])
    detail_transaksi_hotel = dictfetch(cursor)
    context = {'apt' : detail_transaksi_hotel[0]}
    # print(context)
    return render(request, 'detail_transaksi_rs.html', context)

def update_transaksi_rs(request,id):
    username = request.session['username']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("update transaksi_rs set statusbayar = 'Lunas', tanggalpembayaran = now(), waktupembayaran = date_trunc('hour',now()) where idtransaksi=%s;",[id])
    cursor.execute("select * from transaksi_rs t join pasien p on p.nik = t.kodepasien join pengguna_publik pp on p.idpendaftar = pp.username where pp.username=%s;", [username])
    transaksi_rs_response = dictfetch(cursor)
    context = {'apt' : transaksi_rs_response}
    
    return render (request,'transaksi_rs.html', context)

# https://docs.djangoproject.com/en/3.2/topics/db/sql/
def dictfetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
