## Tugas Kelompok Basis Data (F) Reguler
Link website = [https://siruco-f4.herokuapp.com][https://siruco-f4.herokuapp.com]

**Kelompok F4**

**Asisten Dosen = Galangkangin Gotera**

1) Ammar Faridzki Syarif - 1906353712
2) Della Patricia Siregar - 1906399436
3) Fikra Shafna Rahmania - 1906304175
4) Keiko Aaliya Hernowo Putri - 1906298960
5) Muhammad Aulia Adil - 1906398591

Command untuk akses ke database PostgreSQL di Heroku
1) Masuk ke postgreSQL shell terlebih dahulu dan kemudian copas ini
```
psql --host=ec2-54-160-96-70.compute-1.amazonaws.com --port=5432 --username=feyoydaixwjtmu --password --dbname=d3fntfsnrk14oo
```
2) Kemudian akan ada request password dan kemudian masukan password ini
```
9fb195ce2daa56e44de90828e33255e3fac646c9afc9a0754381af4f8d95f183
```

**Untuk WebApp ini, schema yang digunakan adalah 'siruco'**


## Instalasi

Untuk install ke local silahkan clone repository ini
```
git clone https://gitlab.com/aulia-adil/basdat-f4-tk4.git
```
buat virtual environment (dengan env) dan jalankan
```
pip install -r requirements.txt
```
**Aplikasi web ini LANGSUNG tersambung ke DB di AWS RDS, jadi silahkan gunakan command diatas untuk akses ke database PostgreSQL bila perlu**

Silahkan sesuaikan psycopg2 version / type bila ada terjadi error saat instalasi, **tanpa** menggantinya di requirements.txt

## Alur Pengerjaan

1. Lakukan git clone 
```
git clone https://gitlab.com/aulia-adil/basdat-f4-tk4.git
```
2. Buat venv dan aktifkan. Bisa cari di youtube seperti ini ([click here](https://youtu.be/t9KNwBtE5-M))
2. Install requirement yang ada pada project kita dengan cara
```
pip install -r requirements.txt
```
3. Pindah ke branch Trigger1 dan buat branch baru dari sana sesuai dengan trigger temen-temen
```
git checkout Trigger1
```
```
git checkout -b TriggerX
```
4. Runapp Django untuk mengecek apakah lancar
5. Jika lancar, kerjakan sesuai dengan tugas temen-temen dan add commit seperti biasa
6. Push sesuai branch temen-temen
```
git push origin TriggerX
```
7. Kalo mau push ke master, silahkan aja kalo yakin bener. Tapi kalo ragu, bisa create merge request

## Cara connect Postgresql di local

1. Buat user dengan password seperti ini di shell postgresql
```
CREATE USER myuser WITH PASSWORD 'myuserpass';
```
2. Buat database dengan command seperti ini
```
CREATE DATABASE mydb WITH OWNER myuser;
```
3. Connect ke database tersebut
```
\c mydb myuser;
```
4. Import siruco.sql 
```
\i /path/siruco.sql
```
Contoh
```
\i /home/aulia-adil/siruco.sql
```
5. Pastikan konfigurasi database seperti ini di Django settings.py database
```buildoutcfg
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'OPTIONS': {
            'options': '-c search_path=siruco,public'
        },
        'NAME': 'mydb',
        'USER': 'myuser',

        #IF LOCAL
        'PASSWORD': 'myuserpass',
        'HOST': 'localhost',

        #IF PRODUCTION
        # 'PASSWORD': 'uK2WLsH2Jk7t3ynppTQV',
        # 'HOST': 'database-1.c8wtptmvg9b8.us-east-1.rds.amazonaws.com',

        'PORT': '5432',
    }
}
```
6. Lakukan migrasi database di Django
```buildoutcfg
python3 manage.py makemigrations
python3 manage.py migrate
```
7. Jalankan Django
```buildoutcfg
python3 manage.py runserver
```

NOTE: kalo masih ada error bisa chat Adil