import re

from django.db import connection


def is_regex_match(search_key, line):
    pattern = re.compile(search_key)
    return bool(re.match(search_key, str(line)))


def is_password_weak_error(error):
    trigger_error_message = \
        "password harus mengandung uppercase dan angka"
    return is_regex_match(trigger_error_message, error)


def is_duplicate_username_error(error):
    trigger_error_message = \
        "duplicate key value violates unique constraint"
    return is_regex_match(trigger_error_message, error)


def check_password_equal_repeat_password(request):
    password = request.POST.get("password")
    repeat_password = request.POST.get("repeat-password")
    if password != repeat_password:
        raise ValueError()


def execute_query(query):
    cursor = connection.cursor()
    cursor.execute(query)
    if is_regex_match("SELECT", query) or \
       is_regex_match("select", query):
        return cursor.fetchall()


def execute_query_safe(query, data):
    cursor = connection.cursor()
    cursor.execute(query, data)
    if is_regex_match("SELECT", query) or \
       is_regex_match("select", query):
        return cursor.fetchall()


def clean_string_from_select_query(list_string):
    clean_list_string = []
    for string in list_string:
        string = string[0].strip("'")
        string = string.strip("',")
        clean_list_string.append(string)
    return clean_list_string


def delete_one_data_from_database(table_name, key_column, key_name):
    query = ("DELETE FROM {} WHERE"
             " {}='{}'").format(table_name, key_column, key_name)
    execute_query(query)


def convert_request_session_to_dictionary(request):
    result_dict = {}
    counter = 0
    for key, value in request.session.items():
        if counter > 2:
            result_dict[key] = value
        counter += 1
    return result_dict


