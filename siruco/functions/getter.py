from siruco.user_utils.user_selector import get_all_data_from_table_with_key, get_all_username


def get_pengguna_publik_user_data(data):
    username = 0
    nik = 1
    nama = 2
    status = 3
    peran = 4
    no_hp = 5
    return {
        "username": data[username],
        "authority": "pengguna publik",
        "nik": data[nik],
        "nama": data[nama],
        "status": data[status],
        "peran": data[peran],
        "no_hp": data[no_hp],
    }


def get_dokter_user_data(data):
    username = 0
    no_str = 1
    nama = 2
    no_hp = 3
    gelar_depan = 4
    gelar_belakang = 5
    return {
        "username": data[username],
        "authority": "dokter",
        "no_str": data[no_str],
        "nama": data[nama],
        "no_hp": data[no_hp],
        "gelar_depan": data[gelar_depan],
        "gelar_belakang": data[gelar_belakang],
    }


def get_user_data(username):
    is_pengguna_publik = get_all_data_from_table_with_key(username, "username", "PENGGUNA_PUBLIK")
    is_admin_satgas = get_all_data_from_table_with_key(username, "username", "ADMIN_SATGAS")
    is_admin_sistem = get_all_data_from_table_with_key(username, "username", "ADMIN")
    is_dokter = get_all_data_from_table_with_key(username, "username", "DOKTER")

    if len(is_admin_satgas):
        result = get_admin_satgas_user_data(is_admin_satgas[0])
    elif len(is_pengguna_publik):
        result = get_pengguna_publik_user_data(is_pengguna_publik[0])
    elif len(is_dokter):
        result = get_dokter_user_data(is_dokter[0])
    else:
        result = get_admin_sistem_user_data(is_admin_sistem[0])
    return result


def check_authority(username):
    list_admin_satgas = get_all_username("admin_satgas")
    list_pengguna_publik = get_all_username("pengguna_publik")
    list_dokter = get_all_username("dokter")
    if username in list_admin_satgas:
        result = "admin satgas"
    elif username in list_pengguna_publik:
        result = "pengguna publik"
    elif username in list_dokter:
        result = "dokter"
    else:
        result = "admin sistem"
    return result


def get_admin_sistem_user_data(data):
    username = 0
    return {"username": data[username],
            "authority": "admin sistem"}


def get_admin_satgas_user_data(data):
    username = 0
    id_faskes = 1
    return {"username": data[username],
            "authority": "admin satgas",
            "id_faskes": data[id_faskes]}


