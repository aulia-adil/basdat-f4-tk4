from django.urls import path

from crudappointmentwithdoctor.views import list_appointment_view, book_appointment_view, update_appointment_view, \
    buat_appointment_view

urlpatterns = [
    path('list/', list_appointment_view, name='pendaftaran_pasien'),
    path('book/', book_appointment_view, name='mendaftarkan_pasien'),
    path('update/', update_appointment_view, name='list_pasien'),
    path('buat/', buat_appointment_view, name='detail-pasien'),
]
