from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render

from siruco.functions.creator import create_session_data
from siruco.functions.functions import execute_query, execute_query_safe
from siruco.user_utils.user_creator import insert_data


def reformat_jadwal_crjadwaldokter(list_jadwal):
    result_list = []
    counter = 1
    for tuple_element in list_jadwal:
        temp_list = []
        temp_list.append(counter)
        for element in tuple_element:
            temp_list.append(str(element))
        result_list.append(temp_list)
        counter += 1
    return {"list_jadwal": result_list}


def buat_jadwal_dokter_app(request):
    insert_data("JADWAL_DOKTER",
                nostr=request.POST['nostr'],
                username=request.POST['username'],
                jmlpasien=0,
                tanggal=request.POST['tanggal'],
                shift=request.POST['shift'],
                kode_faskes=request.POST['kode_faskes'])
    return list_jadwal_dokter_view(request)


def buat_jadwal_dokter_view(request):
    if request.session['authority'] != 'dokter':
        return HttpResponse(status=404)
    dokter = request.session['username']
    list_jadwal = get_jadwal_crjadwaldokter(dokter)
    list_jadwal = reformat_jadwal_crjadwaldokter(list_jadwal)
    return render(request, "crjadwaldokter/create-jadwal.html", list_jadwal)


def get_jadwal_dokter_crjadwaldokter(dokter):
    query = ("SELECT * FROM JADWAL_DOKTER "
             "WHERE username='{}'").format(dokter)
    query_result = execute_query(query)
    return query_result


def reformat_jadwal_dokter_crjadwaldokter(list_jadwal):
    result_list = []
    counter = 1
    for tuple_element in list_jadwal:
        temp_list = []
        temp_list.append(counter)
        for element in tuple_element:
            temp_list.append(str(element))
        result_list.append(temp_list)
        counter += 1
    return {"list_jadwal_dokter": result_list}


def get_jadwal_crjadwaldokter(dokter):
    query = ("SELECT * "
             "FROM JADWAL "
             "EXCEPT (SELECT kode_faskes, shift, tanggal "
             "FROM JADWAL_DOKTER "
             "WHERE username='{}')").format(dokter)
    return execute_query(query)


def list_jadwal_dokter_view(request):
    if request.session['authority'] != 'dokter':
        return HttpResponse(status=404)
    dokter = request.session['username']
    list_jadwal = get_jadwal_dokter_crjadwaldokter(dokter)
    list_jadwal = reformat_jadwal_dokter_crjadwaldokter(list_jadwal)
    return render(request, "crjadwaldokter/list-jadwal.html", list_jadwal)
