"""siruco URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'trigger3'

urlpatterns = [
    path('create_reservasirumahsakit', views.create_reservasirumahsakit, name='create_reservasirumahsakit'),
    path('daftar_reservasirumahsakit', views.daftar_reservasirumahsakit, name='daftar_reservasirumahsakit'),
    path('create_faskes', views.create_faskes, name='create_faskes'),
    path('daftar_faskes', views.daftar_faskes, name='daftar_faskes'),
    path('detail_faskes', views.detail_faskes, name='detail_faskes'),
    path('delete_faskes', views.delete_faskes, name='delete_faskes'),
    path('update_faskes', views.update_faskes, name='update_faskes'),
    path('create_jadwalfaskes', views.create_jadwalfaskes, name='create_jadwalfaskes'),
    path('daftar_jadwalfaskes', views.daftar_jadwalfaskes, name='daftar_jadwalfaskes'),
    path('create_rumahsakit', views.create_rumahsakit, name='create_rumahsakit'),
    path('daftar_rumahsakit', views.daftar_rumahsakit, name='daftar_rumahsakit'),
    path('update_rumahsakit', views.update_rumahsakit, name='update_rumahsakit'),
    path('daftar_transaksirumahsakit', views.daftar_transaksirumahsakit, name='daftar_transaksirumahsakit'),
]