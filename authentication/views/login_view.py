from django.contrib import auth, messages
from django.contrib.auth import authenticate
from django.contrib.auth import logout
from django.shortcuts import render, redirect

from siruco.functions.creator import create_session_data
from siruco.functions.getter import get_user_data

LOGIN_FAILED_ERROR_MESSAGE = \
    "Email atau password salah"


def login(request):
    return render(request, 'authentication/login.html')


def logging_out(request):
    logout(request)
    return redirect('../login')


def check_account(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    user = authenticate(request, username=username, password=password)
    if user is not None:
        auth.login(request, user)
        create_session_data(request, username)
        result = redirect('../../')
    else:
        messages.error(request, LOGIN_FAILED_ERROR_MESSAGE)
        result = redirect('../login')
    return result
