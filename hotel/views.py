import django.contrib.auth
from django.contrib import auth, messages

from django.contrib.auth import authenticate
from django.db import connection
from django.db.utils import InternalError
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from collections import namedtuple


def create_hotel(request):
    rows_count = count_next_id("hotel", "kode", "hot-")
    next_id = "hot-" + str(rows_count)

    cursor = connection.cursor()
    if request.POST:
        kodehotel = next_id
        namahotel = request.POST.get('namahotel')
        isRujukan = request.POST.get('isRujukan', '0')
        jalan = request.POST.get('jalan')
        kelurahan = request.POST.get('kelurahan')
        kecamatan = request.POST.get('kecamatan')
        kota = request.POST.get('kota')
        provinsi = request.POST.get('provinsi')

        print(isRujukan)
        if '' not in (namahotel, isRujukan, jalan, kelurahan, kecamatan, kota, provinsi):
            query = ("INSERT INTO HOTEL VALUES" "('{}', '{}', '{}', '{}','{}', '{}', '{}', '{}')").format(
                next_id, namahotel, isRujukan, jalan, kelurahan, kecamatan, kota, provinsi)
            cursor.execute(query)
            print("isRujukan : " + isRujukan)
            return redirect('hotel:show-hotel')

        messages.error(
            request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
        return redirect(reverse('hotel:create-hotel'))
    return render(request, 'hotel/create-hotel.html', {'next_id': next_id})


def show_hotel(request):
    cursor = connection.cursor()
    query = (
        'SELECT kode, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov FROM HOTEL')
    cursor.execute(query)
    show_hotel = dictfetchall(cursor)
    context = {'list_hotel': show_hotel}
    print(context)
    return render(request, 'hotel/show-hotel.html', context)


def ubah_hotel(request, kode):
    hotel_kode = str(kode)
    cursor = connection.cursor()
    query = ("SELECT * FROM HOTEL WHERE kode= '"+hotel_kode+"'")
    cursor.execute(query)
    ubah_isi = dictfetchall(cursor)
    print(ubah_isi)
    if request.POST:
        namahotel = request.POST.get('namahotel')
        isRujukan = request.POST.get('isRujukan', '0')
        jalan = request.POST.get('jalan')
        kelurahan = request.POST.get('kelurahan')
        kecamatan = request.POST.get('kecamatan')
        kota = request.POST.get('kota')
        provinsi = request.POST.get('provinsi')
        if '' not in (namahotel, jalan, kelurahan, kecamatan, kota, provinsi):
            cursor = connection.cursor()
            query = ("UPDATE HOTEL SET nama =  '"+namahotel+"', isrujukan =  '"+isRujukan+"', jalan =  '"+jalan+"', kelurahan =  '" +
                     kelurahan+"', kecamatan =  '"+kecamatan+"', kabkot =  '"+kota+"', prov =  '"+provinsi+"' WHERE  kode =  '"+hotel_kode+"'")
            print(query)
            cursor.execute(query)
            return redirect('hotel:show-hotel')
        else:
            messages.error(
                request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('hotel:ubah-hotel', args=(kode,)))
    return render(request, 'hotel/ubah-hotel.html', {'data': ubah_isi[0]})


def create_makan(request):
    cursor = connection.cursor()
    if request.POST:
        kode_hotel = request.POST.get('kode_hotel')
        kode_paket = request.POST.get('kode_paket')
        nama_paket = request.POST.get('nama_paket')
        harga_paket = request.POST.get('harga_paket')

        if '' not in (kode_hotel, kode_paket, nama_paket, harga_paket):
            query = ("INSERT INTO PAKET_MAKAN VALUES"
                     "('{}', '{}', '{}', '{}')").format(kode_hotel, kode_paket, nama_paket, harga_paket)
            print(query)
            cursor.execute(query)
            return redirect(reverse('hotel:show-makan'))
        else:
            print('Ada kosong')
            messages.error(
                request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('hotel:create-makan'))

    query = ("SELECT kode FROM HOTEL")
    cursor.execute(query)
    res = cursor.fetchall()
    list_kode_hotel = [i[0] for i in res]
    print(list_kode_hotel)
    return render(request, 'hotel/create-makan.html', {"list_kode_hotel": list_kode_hotel})


def show_makan(request):
    cursor = connection.cursor()
    query = ('SELECT kodehotel, kodepaket, nama, harga FROM PAKET_MAKAN')
    cursor.execute(query)
    show_makanan = dictfetchall(cursor)

    query2 = (
        "SELECT * FROM PAKET_MAKAN WHERE kodepaket NOT IN (SELECT kodepaket FROM DAFTAR_PESAN WHERE DAFTAR_PESAN.kodehotel = PAKET_MAKAN.kodehotel AND DAFTAR_PESAN.kodepaket = PAKET_MAKAN.kodepaket)")
    cursor.execute(query2)
    show_delete = dictfetchall(cursor)

    context = {'list_makan': show_makanan, 'list_delete': show_delete}
    print(context)
    return render(request, 'hotel/show-makan.html', context)


def ubah_makan(request, kode_hot, kode_pak):
    hotel_kode = str(kode_hot)
    paket_kode = str(kode_pak)
    cursor = connection.cursor()
    query = ("SELECT * FROM PAKET_MAKAN WHERE kodepaket= '" +
             paket_kode+"' AND kodehotel= '"+hotel_kode+"'")
    cursor.execute(query)
    ubah_isi = dictfetchall(cursor)
    print(ubah_isi)
    if request.POST:
        kode_hotel = hotel_kode
        kode_paket = paket_kode
        nama_paket = request.POST.get('nama_paket')
        harga_paket = request.POST.get('harga_paket')
        if '' not in (nama_paket, harga_paket):
            cursor = connection.cursor()
            query = ("UPDATE PAKET_MAKAN SET nama =  '"+nama_paket+"', harga =  '" +
                     harga_paket+"' WHERE kodehotel= '"+kode_hotel+"' AND kodepaket= '"+kode_paket+"'")
            cursor.execute(query)
            return redirect('hotel:show-makan')
        else:
            messages.error(
                request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('hotel:ubah-makan', args=(kode_hot, kode_pak,)))
    return render(request, 'hotel/ubah-makan.html', {'data': ubah_isi[0]})


def delete_makan(request, kode_hot, kode_pak):
    hotel_kode = str(kode_hot)
    paket_kode = str(kode_pak)
    cursor = connection.cursor()
    query = ("SELECT * FROM PAKET_MAKAN WHERE kodepaket= '" +
             paket_kode+"' AND kodehotel= '"+hotel_kode+"' AND kodepaket IN (SELECT kodepaket FROM DAFTAR_PESAN) ")
    # print(query)
    cursor.execute(query)
    ubah_isi = namedtuplefetchall(cursor)
    print(ubah_isi)
    if ubah_isi == []:
        cursor = connection.cursor()
        query = ("DELETE FROM PAKET_MAKAN WHERE kodepaket= '"+paket_kode+"'")
        print("keubah")
        cursor.execute(query)
        messages.success(request, 'Data berhasil dihapus')
        return redirect('hotel:show-makan')
    else:
        print('gak bisa di delete')
        messages.error(
            request, 'Data tidak bisa dihapus')
        return redirect('hotel:show-makan')
    return render(request, 'hotel/show-makan.html')


def create_trans_makan(request):
    rows_count = count_next_id("transaksi_makan", "idtransaksimakan", "idm")
    next_id = "idm0" + str(rows_count)
    if rows_count > 9:
        next_id = "idm" + str(rows_count)

    cursor = connection.cursor()
    query = ("SELECT idtransaksi FROM TRANSAKSI_HOTEL")
    cursor.execute(query)
    res = cursor.fetchall()
    list_idtrans = [i[0] for i in res]
    # print(list_idtrans)

    if request.POST:
        idtrans = request.POST.get('idtransaksi')
        idtransmak = next_id

        query = (
            "SELECT kodehotel from reservasi_hotel natural join transaksi_hotel where idtransaksi='"+idtrans+"' ")
        kodehotel = request.POST.get('kodehotel')

        kode_paket = request.POST.get('kodepaket')

        if '' not in (idtrans, idtransmak, kodehotel, kode_paket):
            query = ("INSERT INTO TRANSAKSI_MAKAN VALUES"
                     "('{}', '{}', '{}', '{}')").format(idtrans, idtransmak, kodehotel, kode_paket)
            print(query)
            cursor.execute(query)
            return redirect(reverse('hotel:show-trans-makan'))
        else:
            print('Ada kosong')
            messages.error(
                request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('hotel:create-trans-makan'))
    context = {"list_idtrans": list_idtrans,
               "next_id": next_id}
    return render(request, 'hotel/create-trans-makan.html', context)


def show_trans_makan(request):
    cursor = connection.cursor()
    query = (
        'SELECT idtransaksi, idtransaksimakan, totalbayar FROM TRANSAKSI_MAKAN')
    cursor.execute(query)
    show_tmakan = dictfetchall(cursor)
    context = {'list_tmakan': show_tmakan}
    print(context)
    return render(request, 'hotel/show-trans-makan.html', context)


def ubah_trans_makan(request):
    return render(request, 'hotel/ubah-trans-makan.html')


def detail_trans_makan(request, idtr, idtm):
    idtransaksi = str(idtr)
    idtransaksimakan = str(idtm)
    cursor = connection.cursor()
    query = (
        "SELECT * FROM DAFTAR_PESAN NATURAL JOIN TRANSAKSI_MAKAN NATURAL JOIN PAKET_MAKAN WHERE idtransaksi= '"+idtransaksi+"' AND idtransaksimakan = '"+idtransaksimakan+"'  ")
    cursor.execute(query)
    detail_transaksi_makan = dictfetchall(cursor)
    context = {'apt': detail_transaksi_makan}
    print(context)
    return render(request, 'hotel/detail-trans-makan.html', context)


def count_next_id(table_name, col_name, prefix):
    with connection.cursor() as cursor:
        cursor.execute("SELECT {} FROM {}".format(col_name, table_name))
        rows = cursor.fetchall()
        if len(rows) == 0:
            return 1
        prefix_len = len(prefix)
        return max(int(row[0][prefix_len:]) for row in rows if row[0][:prefix_len] == prefix) + 1
    return None


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
