from django.urls import path

from authentication.views.login_view import login, check_account, logging_out
from authentication.views.registration_view import registration_admin_sistem, register_user, \
    registration_pengguna_publik, registration_dokter, registration_admin_satgas

urlpatterns = [
    path('register/admin-sistem/',
         registration_admin_sistem, name='register-admin-sistem'),
    path('register/pengguna-publik/',
         registration_pengguna_publik, name='register-pengguna_publik'),
    path('register/dokter/',
         registration_dokter, name='register-dokter'),
    path('register/admin-satgas/',
         registration_admin_satgas, name='register-admin-satgas'),
    path('register-user/', register_user, name='register_user'),
    path('login/', login, name='login'),
    path('check-account/', check_account, name='check-account'),
    path('logout/', logging_out, name='logout'),
]
