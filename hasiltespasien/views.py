import re
from django.contrib import messages
from django.db.utils import IntegrityError
from django.http import response
from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt

#method ini cmn bisa dokter
def buat_hasil_tes(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select nik from pasien")
    nik_response = dictfetch(cursor)
    context = {'apt' : nik_response}

    if(request.method == "POST"): 
        nik_pasien = request.POST.get('nik')
        tanggaltes = request.POST.get('tanggaltes')
        jenis = request.POST.get('jenistes')
        status = request.POST.get('status')
        nilaict = request.POST.get('nilaict')
        if jenis == 'PCR':
            if '' not in(nik_pasien, tanggaltes, jenis, status, nilaict) :
                try:
                    cursor.execute("insert into tes(nik_pasien, tanggaltes, jenis, status, nilaict) values(%s,%s,%s,%s,%s);",[nik_pasien, tanggaltes, jenis, status, nilaict])
                    return redirect('hasiltespasien:list-hasil-tes')
                except IntegrityError:
                    messages.error(request, 'Data sudah ada!')
            else: 
                messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
        else:
            if '' not in(nik_pasien, tanggaltes, jenis, status) :
                try:
                    cursor.execute("insert into tes(nik_pasien, tanggaltes, jenis, status) values(%s,%s,%s,%s);",[nik_pasien, tanggaltes, jenis, status])
                    return redirect('hasiltespasien:list-hasil-tes')
                except IntegrityError:
                    messages.error(request, 'Data sudah ada!')
            else: 
                messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')

    return render(request, 'buat_hasil_tes.html', context)

#method ini bisa dokter, admin satgas, pengguna publik
def list_hasil_tes(request):
    username = request.session['username']
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    if (request.session['authority'] == 'pengguna publik'):
        cursor.execute("select * from tes t join pasien p on p.nik = t.nik_pasien join pengguna_publik pp on pp.username = p.idpendaftar where pp.username=%s", [username])
    else:
        cursor.execute("select * from tes")
    tes_response = dictfetch(cursor)
    context = {'apt': tes_response}
    return render(request, 'list_hasil_tes.html', context)

# https://docs.djangoproject.com/en/3.2/topics/db/sql/
def dictfetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]