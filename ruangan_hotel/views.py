from django import forms
from django.db import connection
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from collections import namedtuple

# def booking_ruangan_hotel(request):
#     cursor = connection.cursor()
#     cursor.execute("set search_path to siruco;")
#     cursor.execute("select kode from hotel;")
#     kh_response = dictfetch(cursor)
#     context = {'apt' : kh_response}

#     if(request.method == "POST"):
#         kodehotel = request.POST.get('kodehotel')
#         jenis_bed = request.POST.get('jenis_bed')
#         tipe = request.POST.get('tipe')
#         harga = request.POST.get('harga')
#         cursor.execute("INSERT INTO HOTEL_ROOM(kodehotel, , jenisbed, tipe, harga")

#     return render(request, 'form_ruangan.html', context)

# def booking_ruangan_hotel(request):
#     cursor = connection.cursor()
#     cursor.execute("set search_path to siruco;")
#     cursor.execute("select kode from hotel;")
#     kh_response = dictfetch(cursor)
#     context = {'apt' : kh_response}

#     if(request.method == "POST"):
#         kodehotel = request.POST.get('kodehotel')
#         jenis_bed = request.POST.get('jenis_bed')
#         tipe = request.POST.get('tipe')
#         harga = request.POST.get('harga')
#         cursor.execute("INSERT INTO HOTEL_ROOM(kodehotel, , jenisbed, tipe, harga")

#     return render(request, 'form_ruangan.html', context)

def dictfetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def count_next_id(table_name, col_name, prefix):
    with connection.cursor() as cursor:
        cursor.execute("SELECT {} FROM {}".format(col_name, table_name))
        rows = cursor.fetchall()
        if len(rows) == 0:
            return 1
        prefix_len = len(prefix)
        return max(int(row[0][prefix_len:]) for row in rows if row[0][:prefix_len] == prefix) + 1
    return None

def booking_ruangan_hotel(request):
    rows_count = count_next_id("hotel_room", "koderoom", "hr-")
    next_id = "hr-" + str(rows_count)
    kh_response = get_kodehotel()
    context = {'apt' : kh_response}
    cursor = connection.cursor()

    if request.POST:
        kodehotel =  request.POST.get('kodehotel') 
        koderoom = next_id
        jenis_bed = request.POST.get('jenisbed')
        tipe = request.POST.get('tipe')
        harga = request.POST.get('harga')
        
        if '' not in (kodehotel, koderoom, jenis_bed, tipe, harga):
            query = ("INSERT INTO HOTEL_ROOM VALUES" "('{}', '{}', '{}', '{}','{}')").format(
                kodehotel, next_id, jenis_bed, tipe, harga)
            cursor.execute(query)
            # return redirect('ruangan_hotel:show_ruangan_hotel')
            return redirect(reverse('ruangan_hotel:show_ruangan_hotel'))

        else:
            messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('ruangan_hotel:booking_ruangan_hotel'))
    return render(request, 'form_ruangan.html', context)

def buat_reservasi_hotel(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select nik from pasien;")
    nik_response = dictfetch(cursor)
    kh_response = get_kodehotel()
    kr_response = get_koderoom()
    context = {'nik' : nik_response, 'kh' : kh_response, 'kr' : kr_response}

    if(request.method == "POST"):
        nik = request.POST.get('nik')
        tanggalmasuk = request.POST.get('tanggalmasuk')
        tanggalkeluar = request.POST.get('tanggalkeluar')
        kodehotel = request.POST.get('kodehotel')
        koderoom = request.POST.get('koderoom')
        if '' not in (nik, tanggalmasuk, tanggalkeluar, kodehotel, koderoom):
            query = ("INSERT INTO RESERVASI_HOTEL VALUES" "('{}', '{}', '{}', '{}','{}')").format(nik, tanggalmasuk, tanggalkeluar, kodehotel, koderoom)
            cursor.execute(query)
            return redirect('ruangan_hotel:show_reservasi_hotel')

        else:
            messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('ruangan_hotel:buat_reservasi_hotel'))

    return render(request, 'reservasi_hotel.html', context)  

def update_transaksi_hotel(request):
    cursor = connection.cursor()
    if(request.method == "POST"):
        statusbayar = request.POST.get('statusbayar')
        if '' not in (statusbayar):
            query = ("UPDATE transaksi_hotel SET statusbayar= WHERE " "('{}')").format(statusbayar)
            cursor.execute(query)
            return redirect('ruangan_hotel:show_transaksi_hotel')
            print('adsadadasda')

        else:
            messages.error(request, 'Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu')
            return redirect(reverse('ruangan_hotel:update_reservasi_hotel'))

    return render(request, 'ubah_transaksi_hotel.html')  

def get_kodehotel():
    cursor = connection.cursor()
    cursor.execute("select kode from hotel;")
    kh_response = dictfetch(cursor)
    return kh_response

def get_koderoom():
    cursor = connection.cursor()
    cursor.execute("select koderoom from hotel h, hotel_room hr WHERE h.kode = hr.kodehotel;") 
    # harus nyambung sm hr
    kr_response = dictfetch(cursor)
    return kr_response

def show_reservasi_hotel(request):
    cursor = connection.cursor()
    query = ('SELECT kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom FROM RESERVASI_HOTEL;')
    cursor.execute(query)
    reservasi_hotel = dictfetchall(cursor)
    context = {'list_reservasi_hotel': reservasi_hotel}
    return render(request, 'list_reservasi_hotel.html', context)

def show_transaksi_hotel(request):
    cursor = connection.cursor()
    query = ('SELECT * FROM TRANSAKSI_HOTEL;')
    cursor.execute(query)
    transaksi_hotel = dictfetchall(cursor)
    context = {'list_transaksi_hotel': transaksi_hotel }
    return render(request, 'list_transaksi_hotel.html', context)

def show_ubah_transaksi_hotel(request, id):
    cursor = connection.cursor()
    query = ("SELECT * FROM TRANSAKSI_HOTEL WHERE idtransaksi=" "('{}')").format(id)
    # query = ("SELECT * FROM TRANSAKSI_HOTEL WHERE idtransaksi=%s", [id])
    cursor.execute(query)
    reservasi_hotel = dictfetchall(cursor)
    context = {'transaksi_hotel': reservasi_hotel[0]}
    return render(request, 'ubah_transaksi_hotel.html', context)

def show_ruangan_hotel(request):
    cursor = connection.cursor()
    query = ('SELECT kodehotel, koderoom, jenisbed, tipe, harga FROM hotel_room;')
    cursor.execute(query)
    reservasi_hotel = dictfetchall(cursor)
    context = {'list_ruangan_hotel': reservasi_hotel}
    return render(request, 'list_ruangan.html', context)

def show_transaksi_booking(request):
    cursor = connection.cursor()
    query = ('SELECT * FROM transaksi_booking;')
    cursor.execute(query)
    transaksi_booking = dictfetchall(cursor)
    context = {'list_transaksi_booking': transaksi_booking }
    return render(request, 'list_transaksi_booking.html', context)

# def delete_room(request, kodehotel, koderoom):
#     hotel_kode = str(kodehotel)
#     room_kode = str(koderoom)
#     cursor = connection.cursor()
#     query = ("SELECT * FROM HOTEL_ROOM WHERE kodehotel= '" +
#              hotel_kode+"' AND koderoom= '"+room_kode+"' ")
#     cursor.execute(query)
#     ubah_isi = dictfetchall(cursor)
#     context = {'list_delete': ubah_isi }
#     if ubah_isi == []:
#         cursor = connection.cursor()
#         query = ("DELETE FROM HOTEL_ROOM WHERE koderoom= '"+room_kode+"'  AND kodehotel= '"+hotel_kode+"' ")
#         cursor.execute(query)
#         messages.success(request, 'Data berhasil dihapus')
#         return redirect('ruangan_hotel:show_ruangan_hotel')
#     else:
#         messages.error(
#             request, 'Data tidak bisa dihapus')
#         return redirect('hotel:show_ruangan_hotel')
#     return render(request, 'list_ruangan_hotel.html')

# def delete_transaksi(request, kodehotel, koderoom):
#     hotel_kode = str(kodehotel)
#     room_kode = str(koderoom)
#     cursor = connection.cursor()
#     query = ("SELECT * FROM HOTEL_ROOM WHERE kodehotel= '" +
#              hotel_kode+"' AND koderoom= '"+room_kode+"' ")
#     cursor.execute(query)
#     ubah_isi = dictfetchall(cursor)
#     if ubah_isi == []:
#         cursor = connection.cursor()
#         query = ("DELETE FROM HOTEL_ROOM WHERE koderoom= '"+room_kode+"'  AND kodehotel= '"+hotel_kode+"' ")
#         cursor.execute(query)
#         messages.success(request, 'Data berhasil dihapus')
#         return redirect('ruangan_hotel:show_ruangan_hotel')
#     else:
#         messages.error(
#             request, 'Data tidak bisa dihapus')
#         return redirect('hotel:show_ruangan_hotel')
#     return render(request, 'list_ruangan_hotel.html')

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]