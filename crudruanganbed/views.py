from django.shortcuts import render, redirect

from siruco.functions.creator import create_session_data
from siruco.functions.functions import execute_query, execute_query_safe


def list_ruangan_view(request):
    query = "SELECT ROW_NUMBER() OVER() AS No, * FROM RUANGAN_RS"
    query_result = execute_query(query)
    return render(request, "crudruanganbed/ruanganrs.html", {"data": query_result})


def list_bed_view(request):
    query = "SELECT ROW_NUMBER() OVER() AS No, * FROM BED_RS"
    query_result = execute_query(query)
    return render(request, "crudruanganbed/bedrs.html", {"data": query_result})


def update_ruangan_view(request):
    if request.POST['simpan'] == "True":
        query = ("UPDATE RUANGAN_RS SET harga=%s, tipe=%s WHERE koders=%s AND koderuangan=%s")
        data = (request.POST['harga-ruangan'], request.POST['tipe'],
                request.POST['kode_rs'], request.POST['kode_ruangan'])
        execute_query_safe(query, data)
        return redirect("/ruangan-bed/list-ruangan/")
    query = ("SELECT tipe, harga FROM RUANGAN_RS WHERE koders=%s "
             "AND koderuangan=%s")
    data = (request.POST['kode_rs'], request.POST['kode_ruangan'])
    query_result = execute_query_safe(query, data)
    context = {
        "kode_rs": request.POST['kode_rs'],
        "kode_ruangan": request.POST['kode_ruangan'],
        "tipe": query_result[0][0],
        "harga_ruangan": query_result[0][1],
    }
    return render(request, "crudruanganbed/update-ruangan.html", context)