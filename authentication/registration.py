from siruco.user_utils.user_creator import create_admin_sistem, create_akun_pengguna, create_dokter, create_admin_satgas, \
    create_pengguna_publik
from siruco.user_utils.user_initializer import create_django_user


def register_admin_sistem(request):
    username = request.POST.get("username")
    password = request.POST.get("password")

    create_akun_pengguna(username, password, "admin")
    create_admin_sistem(username)
    create_django_user(username, password)


def register_pengguna_publik(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    nik = request.POST.get("NIK")
    nama = request.POST.get("nama")
    no_hp = request.POST.get("no-hp")

    create_akun_pengguna(username, password, "pengguna_publik")
    create_pengguna_publik(username, nik, nama, no_hp)
    create_django_user(username, password)


def register_dokter(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    no_str = request.POST.get("no-str")
    nama = request.POST.get("nama")
    no_hp = request.POST.get("no-hp")
    gelar_depan = request.POST.get("gelar-depan")
    gelar_belakang = request.POST.get("gelar-belakang")

    create_akun_pengguna(username, password, "admin")
    create_admin_sistem(username)
    create_dokter(username, no_str, nama,
                  no_hp, gelar_depan, gelar_belakang)
    create_django_user(username, password)


def register_admin_satgas(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    kode_faskes = request.POST.get("kode-faskes")

    create_akun_pengguna(username, password, "admin")
    create_admin_sistem(username)
    create_admin_satgas(username, kode_faskes)
    create_django_user(username, password)