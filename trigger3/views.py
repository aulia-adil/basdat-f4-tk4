from django.shortcuts import render, redirect, reverse
from .forms import *
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import datetime

# Create your views here.
def create_reservasirumahsakit(request):
    # if(request.method == "GET" and request.session["authority"] == "administrator"):
    # cursor = connection.cursor()
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        create_reservasirumahsakit = CreateReservasiRumahSakit()
        # test = request.GET['dropdown_kode_faskes']
        # print(test)
        response = {'create_form_reservasirumahsakit': create_reservasirumahsakit}
        return render(request, 'create_reservasirumahsakit.html', response)
    elif(request.method == "POST"):
        cursor = connection.cursor()
        kodepasien = request.POST["dropdown_pasien"]
        tglmasuk = request.POST["tanggal_masuk"]
        tglkeluar = request.POST["tanggal_keluar"]
        koders = request.POST["dropdown_kode_faskes"]
        koderuangan = request.POST["dropdown_koderuangan"]
        kodebed = request.POST["dropdown_kodebed"]
        # print(kodepasien)
        # print(tglmasuk)
        # print(tglkeluar)
        # print(koders)
        # print(koderuangan)
        # print(kodebed)

        try:
            cursor.execute(f"INSERT INTO siruco.reservasi_rs (kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed) VALUES ('{kodepasien}', '{tglmasuk}', '{tglkeluar}', '{koders}', '{koderuangan}', '{kodebed}')")
            messages.info(request, "Reservasi berhasil ditambahkan")
        except:
            messages.error(request, "Lengkapi data terlebih dahulu")
            return redirect(reverse("trigger3:create_reservasirumahsakit"))
        return redirect(reverse("trigger3:daftar_reservasirumahsakit"))
    return redirect(reverse("trigger3:daftar_reservasirumahsakit"))

def daftar_reservasirumahsakit(request):
    response = {}
    data = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["authority"] == "admin satgas" or request.session["authority"] == "pengguna publik"):
        cursor.execute(f"SELECT * FROM siruco.reservasi_rs")
        dataFetch = cursor.fetchall()

        for i in dataFetch:
            data.append({
                "kodepasien" : f"{i[0]}",
                "tglmasuk" : f"{i[1]}",
                "tglkeluar": f"{i[2]}",
                "koders" : f"{i[3]}",
                "koderuangan": f"{i[4]}",
                "kodebed": f"{i[5]}",
            })
        response = {'daftar_reservasirumahsakit': data}
        return render(request, 'daftar_reservasirumahsakit.html',response)
    return redirect(reverse("trigger3:daftar_reservasirumahsakit"))

def create_faskes(request):
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        create_faskes = CreateFaskes()
        cursor.execute(f"SELECT * FROM siruco.faskes ORDER BY kode DESC LIMIT 1")
        select = cursor.fetchone()
        if(select):
            data = {
                "kode": f"{select[0]}",
            }
        else:
            return redirect(reverse("trigger3:daftar_faskes"))
        print(data)
        temp = data.get('kode')
        newKode = int(temp) + 1
        data.update({"newKode": newKode})
        response = {'create_form_faskes': create_faskes}
        response['data_faskes'] = data
        return render(request, 'create_faskes.html', response)
    elif(request.method == "POST" and request.session["authority"] == "admin satgas"):

    # elif(request.method == "POST"):
        kode_faskes = request.POST["kode"]
        tipe = request.POST["tipe"]
        status = request.POST["status"]
        nama = request.POST["nama_faskes"]
        jalan = request.POST["jalan"]
        kelurahan = request.POST["kelurahan"]
        kecamatan = request.POST["kecamatan"]
        kabupaten_kota = request.POST["kabupaten_kota"]
        provinsi = request.POST["provinsi"]
        try:
            cursor.execute(f"INSERT INTO siruco.faskes VALUES ('{kode_faskes}','{tipe}', '{nama}', '{status}', '{jalan}', '{kelurahan}', '{kecamatan}', '{kabupaten_kota}', '{provinsi}')")
            messages.info(request, "Faskes berhasil ditambahkan")
        except:
            messages.error(request, "Lengkapi data terlebih dahulu")
            return redirect(reverse("trigger3:create_faskes"))
        return redirect(reverse("trigger3:daftar_faskes"))
    return redirect(reverse("trigger3:daftar_faskes"))


def daftar_faskes(request):
    response = {}
    data = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        cursor.execute(f"SELECT * FROM siruco.faskes ORDER BY kode")
        dataFetch = cursor.fetchall()
        for i in dataFetch:
            data.append({
                "kode" : f"{i[0]}",
                "tipe" : f"{i[1]}",
                "nama_faskes" : f"{i[2]}",
            })

        response = {'daftar_faskes': data}
        return render(request, 'daftar_faskes.html',response)
    return redirect(reverse("trigger3:daftar_faskes"))

def detail_faskes(request):
    response = {}
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        try:
            if(request.GET['kode']):
                pass
        except:
            return redirect(reverse("trigger3:daftar_faskes"))

        kode = request.GET['kode']
        cursor.execute(f"SELECT * FROM siruco.faskes where kode = '{kode}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "kode": f"{select[0]}",
                "tipe": f"{select[1]}",
                "nama": f"{select[2]}",
                "status": f"{select[3]}",
                "jalan": f"{select[4]}",
                "kelurahan": f"{select[5]}",
                "kecamatan": f"{select[6]}",
                "kabupaten": f"{select[7]}",
                "provinsi": f"{select[8]}",
            }
            # print(data)
        else:
            return redirect(reverse("trigger3:daftar_faskes"))

        print(data)
        response = {'data_faskes': data}
        return render(request,'detail_faskes.html',response)

def delete_faskes(request):
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        cursor = connection.cursor()
        kode = request.GET["kode"]
        try:
            cursor.execute(f"DELETE from siruco.faskes where kode = '{kode}'")
            messages.info(request,"Faskes Berhasil di Hapus")
        except:
            messages.error(request, "RS Cabang Gagal di Hapus")
            return redirect(reverse("trigger3:daftar_faskes"))
        return redirect(reverse("trigger3:daftar_faskes"))
    return redirect(reverse("trigger3:pengguna"))

def create_jadwalfaskes(request):
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        create_jadwalfaskes = CreateJadwalFaskes()
        response = {'create_form_jadwalfaskes': create_jadwalfaskes}
        return render(request, 'create_jadwalfaskes.html', response)
    elif(request.method == "POST"):
        cursor = connection.cursor()
        kode_faskes = request.POST["dropdown_faskes"]
        shift = request.POST["shift"]
        tanggal = request.POST["tanggal"]
        # print(kode_faskes)
        # print(shift)
        # print(tanggal)

        try:
            if(len(shift) > 0 and tanggal is not None):
                cursor.execute(f"INSERT INTO siruco.jadwal (kode_faskes, shift, tanggal) VALUES ('{kode_faskes}', '{shift}', '{tanggal}')")
                messages.info(request, "Jadwal berhasil ditambahkan")
            elif(len(shift) == 0):
                messages.error(request, "Lengkapi data terlebih dahulu")
                return redirect(reverse("trigger3:create_jadwalfaskes"))
        except:
            messages.error(request, "Lengkapi data terlebih dahulu")
            return redirect(reverse("trigger3:create_jadwalfaskes"))
        return redirect(reverse("trigger3:daftar_jadwalfaskes"))
    return redirect(reverse("trigger3:daftar_jadwalfaskes"))

def daftar_jadwalfaskes(request):
    response = {}
    data = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        cursor.execute(f"SELECT * FROM siruco.jadwal ORDER BY kode_faskes")
        dataFetch = cursor.fetchall()
        for i in dataFetch:
            data.append({
                "kode_faskes" : f"{i[0]}",
                "shift" : f"{i[1]}",
                "tanggal": f"{i[2]}"
            })
        # data = cursor.fetchone()
        # print(data)
        response = {'daftar_jadwalfaskes': data}
        return render(request, 'daftar_jadwalfaskes.html',response)
    return redirect(reverse("trigger3:daftar_jadwalfaskes"))

def create_rumahsakit(request):
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        create_rs = CreateRumahSakitRujukan()
        response = {'create_rumahsakit': create_rs}
        return render(request, 'create_rumahsakit.html', response)
    elif(request.method == "POST"):
        cursor = connection.cursor()
        kode_faskes = request.POST["dropdown_kode_faskes"]
        rujukan = request.POST.get("rujukan")
        rujukanData = 0
        print(rujukan)
        print(kode_faskes)
        try:
            if (rujukan == "on"):
                rujukanData = 1
                cursor.execute(f"INSERT INTO siruco.rumah_sakit VALUES ('{kode_faskes}','{rujukanData}')")
                messages.info(request, "Create Rumah Sakit Berhasil Dibuat")
            elif (rujukan is None):
                rujukanData = 0
                cursor.execute(f"INSERT INTO siruco.rumah_sakit VALUES ('{kode_faskes}','{rujukanData}')")
                messages.info(request, "Create Rumah Sakit Berhasil Dibuat")
            else:
                pass
        except:
            messages.error(request, "Data Belum Lengkap atau Data Sudah Pernah Ditambahkan")
            return redirect(reverse("trigger3:create_rumahsakit"))
        return redirect(reverse("trigger3:daftar_rumahsakit"))
    return redirect(reverse("trigger3:daftar_rumahsakit"))


def daftar_rumahsakit(request):
    response = {}
    data = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        cursor.execute(f"SELECT * FROM siruco.rumah_sakit ORDER BY kode_faskes")
        dataFetch = cursor.fetchall()
        for i in dataFetch:
            data.append({
                "kode_faskes" : f"{i[0]}",
                "rujukan" : f"{i[1]}"
            })
        # data = cursor.fetchone()
        # print(data)
        response = {'daftar_rumahsakit': data}
        return render(request, 'daftar_rumahsakit.html',response)
    return redirect(reverse("trigger3:daftar_rumahsakit"))


def update_rumahsakit(request):
    response = {}
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        update_rumahsakit = UpdateRumahSakitRujukan()
        response = {
            'update_form' : update_rumahsakit,
        }
        try:
            if(request.GET['kode_faskes']):
                pass
        except:
            return redirect(reverse("trigger3:daftar_rumahsakit"))

        kode_faskes = request.GET['kode_faskes']
        cursor.execute(f"SELECT * FROM siruco.rumah_sakit where kode_faskes = '{kode_faskes}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "kode_faskes": f"{select[0]}",
                "rujukan": f"{select[1]}",
            }
            # print(data)
        else:
            return redirect(reverse("trigger3:daftar_rumahsakit"))
        # response = {'rumahsakit': data}
        response['data_update'] = data
        return render(request,'update_rumahsakit.html',response)
        
    elif(request.method == "POST" and request.session["authority"] == "admin satgas"):
        cursor = connection.cursor()
        kode_faskes = request.POST["kode_faskes"]
        rujukan = request.POST.get("rujukan")
        rujukanData = 0
        print(kode_faskes)
        print(rujukan)
        try:
            if(rujukan == "on"):
                rujukanData = 1
                cursor.execute(f"UPDATE siruco.rumah_sakit SET isirujukan = '{rujukanData}' WHERE kode_faskes = '{kode_faskes}'")
                messages.info(request,"Update Rumah Sakit Berhasil")
            elif(rujukan is None):
                rujukanData = 0
                cursor.execute(f"UPDATE siruco.rumah_sakit SET isirujukan = '{rujukanData}' WHERE kode_faskes = '{kode_faskes}'")
                messages.info(request,"Update Rumah Sakit Berhasil")
        except:
            messages.error(request,"Update rumah sakit gagal, coba ulangi lagi")
            return redirect(reverse("trigger3:update_rumahsakit"))
        return redirect(reverse("trigger3:daftar_rumahsakit"))
    return redirect(reverse("trigger3:daftar_rumahsakit"))


def daftar_transaksirumahsakit(request):
    response = {}
    data = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        cursor.execute(f"SELECT * FROM siruco.transaksi_rs")
        dataFetch = cursor.fetchall()

        for i in dataFetch:
            data.append({
                "idtransaksi" : f"{i[0]}",
                "kodepasien" : f"{i[1]}",
                "tanggalpembayaran": f"{i[2]}",
                "waktupembayaran" : f"{i[3]}",
                "tglmasuk": f"{i[4]}",
                "totalbiaya": f"{i[5]}",
                "statusbayar": f"{i[6]}",
            })
        response = {'daftar_transaksirumahsakit': data}
        return render(request, 'daftar_transaksirumahsakit.html',response)
    return redirect(reverse("trigger3:daftar_transaskirumahsakit"))

def update_faskes(request):
    response = {}
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["authority"] == "admin satgas"):
        try:
            if(request.GET['kode']):
                pass
        except:
            return redirect(reverse("trigger3:daftar_faskes"))

        kode = request.GET['kode']
        cursor.execute(f"SELECT * FROM siruco.faskes where kode = '{kode}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "kode": f"{select[0]}",
                "tipe": f"{select[1]}",
                "nama": f"{select[2]}",
                "status": f"{select[3]}",
                "jalan": f"{select[4]}",
                "kelurahan": f"{select[5]}",
                "kecamatan": f"{select[6]}",
                "kabupaten": f"{select[7]}",
                "provinsi": f"{select[8]}",
            }
            # print(data)
        else:
            return redirect(reverse("trigger3:daftar_faskes"))
        # response = {'rumahsakit': data}
        response['data'] = data
        return render(request,'update_faskes.html',response)
        
    elif(request.method == "POST" and request.session["authority"] == "admin satgas"):
        cursor = connection.cursor()
        kode = request.POST.get("kode")
        tipe = request.POST["tipe"]
        nama = request.POST["nama"]
        status = request.POST["status"]
        jalan = request.POST["jalan"]
        kelurahan = request.POST["kelurahan"]
        kecamatan = request.POST["kecamatan"]
        kabupaten = request.POST.get("kabupaten")
        provinsi = request.POST["provinsi"]

        try:
            cursor.execute(f"UPDATE siruco.faskes SET tipe = '{tipe}',nama = '{nama}',statusmilik = '{status}',jalan = '{jalan}',kelurahan = '{kelurahan}',kecamatan = '{kecamatan}',kabkot = '{kabupaten}',prov = '{provinsi}' WHERE kode = '{kode}'")
            messages.info(request,"Update Faskes Berhasil")
        except:
            messages.error(request,"Update Faskes gagal, coba ulangi lagi")
            return redirect(reverse("trigger3:update_faskes"))
        return redirect(reverse("trigger3:daftar_faskes"))
    return redirect(reverse("trigger3:daftar_faskes"))
