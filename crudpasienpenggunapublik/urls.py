from django.urls import path

from crudpasienpenggunapublik.views import pendaftaran_pasien_view, mendaftarkan_pasien, list_pasien_view, \
    detail_pasien, delete_pasien, update_pasien_view, update_pasien_app

urlpatterns = [
    path('pendaftaran-pasien/', pendaftaran_pasien_view, name='pendaftaran_pasien'),
    path('pendaftaran-pasien/daftar', mendaftarkan_pasien, name='mendaftarkan_pasien'),
    path('list-pasien/', list_pasien_view, name='list_pasien'),
    path('list-pasien/detail-pasien', detail_pasien, name='detail-pasien'),
    path('list-pasien/delete-pasien', delete_pasien, name='delete-pasien'),
    path('list-pasien/update-pasien', update_pasien_view, name='update-pasien'),
    path('list-pasien/update-pasien/update', update_pasien_app, name='update-pasien'),
]
