from django.urls import path

from crudruanganbed.views import list_ruangan_view, list_bed_view, update_ruangan_view

urlpatterns = [
    path('list-ruangan/', list_ruangan_view, name='update_personal_data'),
    path('list-bed/', list_bed_view, name='update_personal_data'),
    path('list-ruangan/update/', update_ruangan_view, name='update_personal_data'),
]
