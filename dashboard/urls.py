from django.urls import path

from dashboard.views import dashboard, update_personal_data

urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('update-personal-data', update_personal_data, name='update_personal_data'),
]
