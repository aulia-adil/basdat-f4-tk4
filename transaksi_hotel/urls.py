from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from . import views

app_name='transaksi_hotel'

urlpatterns = [
    path('transaksi_hotel',views.display_transaksi_hotel,name='display-transaksi-hotel'),
    path('transaksi_hotel/detail/<str:id>',views.detail_transaksi_hotel,name='detail-transaksi-hotel'),
    path('transaksi_hotel/<str:id>',views.update_transaksi_hotel,name='update-transaksi-hotel'),
]