from siruco.functions.functions import execute_query


def update_user(username, table, **new_data):
    query_new_data = ""
    for column, datum in new_data.items():
        query_new_data += "{}='{}', ".format(column, datum)
    query_new_data = query_new_data[0:len(query_new_data)-2] + " "
    query = ("UPDATE {} "
             "SET {} "
             "WHERE username='{}'").format(table, query_new_data, username)
    execute_query(query)


def update_pasien(nik, **new_data):
    query_new_data = ""
    for column, datum in new_data.items():
        query_new_data += "{}='{}', ".format(column, datum)
    query_new_data = query_new_data[0:len(query_new_data)-2] + " "
    query = ("UPDATE PASIEN "
             "SET {} "
             "WHERE nik='{}'").format(query_new_data, nik)
    execute_query(query)