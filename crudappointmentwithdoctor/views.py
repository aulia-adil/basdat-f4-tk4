from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import InternalError, IntegrityError
from django.shortcuts import render, redirect

from crudpasienpenggunapublik.views import INPUT_NOT_COMPLETE_ERROR
from siruco.functions.creator import create_session_data
from siruco.functions.functions import execute_query, execute_query_safe
from siruco.user_utils.user_creator import insert_data


@login_required(login_url='login')
def list_appointment_view(request):
    if request.POST:
        if request.POST['delete']:
            query = ("DELETE FROM MEMERIKSA WHERE "
                     "nik_pasien=%s AND "
                     "username_dokter=%s AND "
                     "kode_faskes=%s AND "
                     "praktek_shift=%s AND "
                     "praktek_tgl=%s")
            execute_query_safe(query, (
                request.POST['nik'],
                request.POST['username_dokter'],
                request.POST['kode_faskes'],
                request.POST['shift'],
                request.POST['tanggal'],
            ))
    query = ("SELECT ROW_NUMBER() OVER() AS No, nik_pasien, username_dokter, kode_faskes, "
             "praktek_shift, praktek_tgl, rekomendasi FROM MEMERIKSA")
    result = execute_query(query)
    new_list = []
    for elements in result:
        temp_list = []
        for element in elements:
            temp_list.append(str(element))
        new_list.append(temp_list)
    return render(request, "crudappointmentwithdoctor/list-appointment.html", {"data": new_list})


@login_required(login_url='login')
def get_all_pasien_nik():
    query = ("SELECT nik FROM PASIEN")
    result = execute_query(query)
    new_list = []
    for element in result:
        new_list.append(element[0])
    return new_list


@login_required(login_url='login')
def get_pasien_nik(username):
    query = ("SELECT nik FROM PASIEN WHERE idpendaftar=%s")
    result = execute_query_safe(query, (username,))
    new_list = []
    for element in result:
        new_list.append(element[0])
    return new_list


JUMLAH_PASIEN_PENUH_ERROR = \
    "Silahkan pilih shift dan tanggal lainnya, \
    karena shift dan tanggal yang dipilih sudah penuh"
PASIEN_SUDAH_BOOKING_ERROR = \
    "Pasien ini sudah booking dengan dokter dan sesi ini"


@login_required(login_url='login')
def book_appointment_view(request):
    if request.POST['simpan'] == "True":
        try:
            insert_data("MEMERIKSA",
                        nostr=request.POST['nostr'],
                        username_dokter=request.POST['email-dokter'],
                        kode_faskes=request.POST['kode_faskes'],
                        praktek_shift=request.POST['shift'],
                        praktek_tgl=request.POST['tanggal'],
                        nik_pasien=request.POST['nik']
                        )
            return redirect("/appointment/list/")
        except InternalError:
            messages.error(request, JUMLAH_PASIEN_PENUH_ERROR)
        except IntegrityError:
            messages.error(request, PASIEN_SUDAH_BOOKING_ERROR)

    context = {
        "email_dokter": request.POST['email-dokter'],
        "nostr": request.POST['nostr'],
        "kode_faskes": request.POST['kode_faskes'],
        "shift": request.POST['shift'],
        "tanggal": request.POST['tanggal'],
    }
    if request.session['authority'] == "admin satgas":
        context['list_nik'] = get_all_pasien_nik()
    elif request.session['authority'] == "pengguna publik":
        context['list_nik'] = get_pasien_nik(request.session['username'])
    return render(request, "crudappointmentwithdoctor/book-appointment.html", context)


def isUpdateMemeriksaInputComplete(request):
    count = 0
    for value in request.POST.values():
        if value:
            count += 1
    return count == 8


@login_required(login_url='login')
def update_appointment_view(request):
    if request.POST['simpan'] == "True":
        if isUpdateMemeriksaInputComplete(request):
            query = ("UPDATE MEMERIKSA SET rekomendasi=%s "
                     "WHERE nik_pasien=%s AND kode_faskes=%s AND username_dokter=%s AND "
                     "praktek_tgl=%s AND "
                     "praktek_shift=%s")
            data = (request.POST['rekomendasi'],
                    request.POST['nik'],
                    request.POST['kode_faskes'],
                    request.POST['username_dokter'],
                    request.POST['tanggal'],
                    request.POST['shift'],
                    )
            execute_query_safe(query, data)
            return redirect("/appointment/list/")
        else:
            messages.error(request, INPUT_NOT_COMPLETE_ERROR)

    context = {
        "nik": request.POST['nik'],
        "email_dokter": request.POST['username_dokter'],
        "kode_faskes": request.POST['kode_faskes'],
        "shift": request.POST['shift'],
        "tanggal": request.POST['tanggal'],
        "rekomendasi": request.POST['rekomendasi']
    }
    return render(request, "crudappointmentwithdoctor/update-appointment.html", context)


@login_required(login_url='login')
def buat_appointment_view(request):
    query = ("SELECT "
             "ROW_NUMBER() OVER() AS No, nostr, username, kode_faskes, shift, tanggal "
             "FROM JADWAL_DOKTER")
    result = execute_query(query)
    new_list = []
    for elements in result:
        temp_list = []
        for element in elements:
            temp_list.append(str(element))
        new_list.append(temp_list)
    return render(request, "crudappointmentwithdoctor/buat-appointment.html", {"list_jadwal": new_list})
